import mongoose from 'mongoose';

const connect = (): Promise<typeof mongoose> => {
  mongoose.connection.on('connecting', () => {
    console.info('Connecting to MongoDB...');
  });
  mongoose.connection.on('error', error => {
    console.error(`Error in MongoDb connection: ${error}`);
    mongoose.disconnect();
  });
  mongoose.connection.on('connected', () => {
    console.info('MongoDB connected!');
  });
  mongoose.connection.once('open', () => {
    console.info('MongoDB connection opened!');
  });
  mongoose.connection.on('disconnected', async () => {
    console.info('MongoDB disconnected!');
  });

  return mongoose.connect(process.env.MONGO_URI as string, {
    autoIndex: true,
  });
};

export { connect };
