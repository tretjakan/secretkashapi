import mongoose from 'mongoose';

/**
 * Users
 */
const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
});

interface IUser {
  username: string;
  password: string;
}

/**
 * Models
 */
const User = mongoose.model('users', userSchema, 'users');

export { User, IUser };
