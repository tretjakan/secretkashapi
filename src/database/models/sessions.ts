import mongoose from 'mongoose';

/**
 * Sessions
 */
const configurationSchema = new mongoose.Schema({
  amountOfPlayers: Number,
});

const ruleSchema = new mongoose.Schema({
  description: String,
  type: {
    type: String,
    enum: ['liberal', 'nazi'],
    required: true,
  },
});

const sessionSchema = new mongoose.Schema({
  users: [
    {
      id: {
        type: String,
        required: true,
      },
      type: {
        type: String,
        enum: ['pending', 'ready', 'eliminated'],
        required: true,
      },
      username: {
        type: String,
        required: true,
      },
      role: String,
    },
  ],
  status: {
    type: String,
    enum: ['closed', 'pending', 'active'],
    required: true,
  },
  configuration: configurationSchema,
  name: {
    type: String,
    required: true,
  },
  rules: [ruleSchema],
});

interface ISession {
  users: {
    id: string;
    type: 'pending' | 'ready' | 'eliminated';
    role?: string;
    username: string;
  }[];
  isActive: boolean;
  configuration: {
    amountOfPlayers: number;
  };
}

const Session = mongoose.model('sessions', sessionSchema, 'sessions');

export { Session, ISession };
