export const MAX_USERS = 8;
export const MIN_USERS = 2;

export const Events = {
  sendMessage: 'sendMessage',
  sendMessageToRoom: 'sendMessageToRoom',
};
