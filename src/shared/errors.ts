interface CustomErrorInstance {
  status: number;
  message: string;
  description?: string;
}

export class CustomError implements CustomErrorInstance {
  status: number;

  message: string;

  description?: string;

  constructor(message: string, status = 500, description = '') {
    this.message = message;
    this.status = status;
    if (description) {
      this.description = description;
    }
    Object.setPrototypeOf(this, CustomError.prototype);
  }
}
