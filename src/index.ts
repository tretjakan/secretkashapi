import 'module-alias/register';
import express, { Express } from 'express';
import 'express-async-errors';
import cors from 'cors';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import compression from 'compression';
import * as http from 'http';
import * as socketio from 'socket.io';
dotenv.config();

import { connect } from './database/connect';
import setupRouters from './modules';
import { errorHandler } from './middleware/error-handler';
import { events } from './modules/events';
import { requestLogger } from './middleware/request-logger';

const app: Express = express();

const server = http.createServer(app);
const io = new socketio.Server(server);

app.use(compression());
app.use(cors());
app.use(requestLogger);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

connect().then(() => {
  setupRouters(app);
  app.use(errorHandler);
});

events(io);

server.listen(process.env.PORT, () => {
  console.log(`[Kash Server is running]`);
});
