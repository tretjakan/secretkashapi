import { Express } from 'express';
import { basicRouter } from './basic';
import { authRouter } from './auth';
import { usersRouter } from './users';
import { sessionsRouter } from './sessions';

const setup = (app: Express) => {
  basicRouter(app);
  authRouter(app);
  usersRouter(app);
  sessionsRouter(app);
};

export default setup;
