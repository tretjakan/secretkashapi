import { ObjectId } from 'mongodb';
import { Request, Response } from 'express';
import { User } from '@models';

const users = {
  get: async (req: Request, res: Response) => {
    const id = req.params.userId;

    const user = await User.findOne({ _id: new ObjectId(id) }).select(['username']);

    if (!user) {
      return res.status(400).send({ message: `User with id:${id} does not exist` });
    }

    return res.status(200).send({ data: user });
  },
};

export { users };
