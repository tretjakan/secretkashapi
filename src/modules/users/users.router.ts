import { Express } from 'express';
import { authMiddleware } from '@middleware/auth';

import { users } from './users.service';

const usersRouter = (app: Express) => {
  app.get('/users/:userId', authMiddleware, users.get);
};

export { usersRouter };
