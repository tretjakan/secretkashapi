import { Express, Request, Response } from 'express';

const basicRouter = (app: Express) => {
  app.get('/health', (req: Request, res: Response) => {
    res.send(`Health check for ${req.hostname} success`);
  });
};

export { basicRouter };
