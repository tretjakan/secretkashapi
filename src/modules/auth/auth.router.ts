import { Express } from 'express';

import { signUp, login } from './auth.service';

const authRouter = (app: Express) => {
  app.post('/signup', signUp);
  app.get('/login', login);
};

export { authRouter };
