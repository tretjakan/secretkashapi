import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { User, IUser } from '@models';

const signUp = async (req: Request, res: Response) => {
  const body: Partial<IUser> = { username: req.body.username };
  body.password = await bcrypt.hash(req.body.password, 10);

  const user = await User.create(body);
  return res.status(200).send({ data: user });
};

const login = async (req: Request, res: Response) => {
  const username = req.query.username;
  const password = req.query.password as string;

  const user = await User.findOne({ username });

  if (!user) {
    return res.status(400).send({ message: `User with username:${username} does not exist` });
  }

  const parsePassword = await bcrypt.compare(password, user.password);

  if (!parsePassword) {
    return res.status(400).json({ message: "Password doesn't match" });
  }

  const token = jwt.sign({ username: user.username }, process.env.SECRET as string);

  return res.status(200).send({ data: { token, user } });
};

export { login, signUp };
