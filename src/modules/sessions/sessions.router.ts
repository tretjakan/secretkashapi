import { Express } from 'express';
import { authMiddleware } from '@middleware/auth';

import { sessions } from './sessions.service';

const sessionsRouter = (app: Express) => {
  app.post('/sessions', authMiddleware, sessions.create);
  app.patch('/sessions/:sessionName', authMiddleware, sessions.patch);
  app.patch('/sessions/:sessionName/users/:userId', authMiddleware, sessions.patchPlayer);
  app.get('/sessions/:sessionId', authMiddleware, sessions.get);
};

export { sessionsRouter };
