import { ISession } from '@models';

enum RuleTypes {
  nazi = 'nazi',
  liberal = 'liberal',
  dictator = 'dictator',
}

const matrix: { [key: string]: { [key: string]: number } } = {
  3: {
    [RuleTypes.dictator]: 1,
    [RuleTypes.liberal]: 2,
  },
  5: {
    [RuleTypes.liberal]: 3,
    [RuleTypes.dictator]: 1,
    [RuleTypes.nazi]: 1,
  },
  6: {
    [RuleTypes.liberal]: 4,
    [RuleTypes.dictator]: 1,
    [RuleTypes.nazi]: 1,
  },
  7: {
    [RuleTypes.liberal]: 4,
    [RuleTypes.dictator]: 1,
    [RuleTypes.nazi]: 2,
  },
  8: {
    [RuleTypes.liberal]: 5,
    [RuleTypes.dictator]: 1,
    [RuleTypes.nazi]: 2,
  },
};

const shuffleArray = (array: string[]) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    const temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
  return array;
};

const getRole = (users: Pick<ISession, 'users'>['users'], amountOfPlayers: number) => {
  const model = matrix[amountOfPlayers];

  const existingRoles = users.map(user => user.role);
  const rolesLeft = Object.keys(model).reduce((acc: any, curr: any) => {
    const rolesExist = existingRoles.filter(role => role === curr);

    return {
      ...acc,
      [curr]: model[curr] - rolesExist.length,
    };
  }, {});

  const roles = shuffleArray(
    Object.keys(rolesLeft)
      .map(key => new Array(rolesLeft[key]).fill(key))
      .flat(),
  );

  return roles[0];
};

export { getRole };
