import { uniqueNamesGenerator, adjectives, colors, animals } from 'unique-names-generator';
import { Request, Response } from 'express';
import { ObjectId } from 'mongodb';

import { Events, MAX_USERS, MIN_USERS } from '@shared/enums';
import { User, Session, ISession } from '@models';
import { eventEmitter } from '../events/emitter';
import { getRole } from './sessions.utils';

const sessions = {
  create: async (req: Request, res: Response) => {
    const { username } = req.user;
    const user = await User.findOne({ username }).select(['username']);

    if (!user) {
      return res.status(400).send({ message: 'Cannot find user' });
    }

    const amountOfPlayers = parseInt(req.body.amountOfPlayers, 10);

    if (Number.isNaN(amountOfPlayers)) {
      return res.status(400).send({ message: 'amountOfPlayers is not valid' });
    }

    if (amountOfPlayers < MIN_USERS || amountOfPlayers > MAX_USERS) {
      return res.status(400).send({
        message: `amountOfPlayers should be less or equal ${MAX_USERS} and bigger then ${MIN_USERS}`,
      });
    }

    const body = {
      name: uniqueNamesGenerator({
        dictionaries: [colors, animals],
      }),
      configuration: { amountOfPlayers },
      status: 'pending',
      users: [
        {
          id: user._id.toString(),
          type: 'pending',
          role: getRole([], amountOfPlayers),
          username: user.username,
        },
      ],
    };

    const session = await Session.create(body);

    return res.status(200).send({ data: session });
  },
  patch: async (req: Request, res: Response) => {
    const { sessionName } = req.params;
    const { userId } = req.body;
    const { type } = req.query;

    const [user, session] = await Promise.all([
      User.findOne({ _id: new ObjectId(userId) }).select(['username']),
      Session.findOne({ name: sessionName }),
    ]);

    if (!user) {
      console.log('error- 1');
      return res.status(400).send({ message: 'User does not exist' });
    }

    if (!session) {
      console.log('error- 2');
      return res.status(400).send({ message: 'Cannot find session' });
    }

    if (session.status === 'closed') {
      console.log('error- 3');
      return res.status(400).send({ message: 'Session closed' });
    }

    if (
      type === 'join' &&
      (session.users.length >= MAX_USERS ||
        session.users.length >= (session.configuration?.amountOfPlayers as number))
    ) {
      return res.status(400).send({ message: 'Amount of users exceeds' });
    }

    const users =
      type === 'join'
        ? [
            ...new Set(
              session?.users.concat([
                {
                  id: userId,
                  type: 'pending',
                  role: getRole(session.users, session.configuration?.amountOfPlayers as number),
                  username: user.username,
                },
              ]),
            ),
          ]
        : session?.users.filter(user => user.id !== userId);

    const getStatus = () => {
      if (!users.length || (session.status === 'active' && type === 'leave')) {
        return 'closed';
      }
      if (users.length === session?.configuration?.amountOfPlayers) {
        return 'active';
      }
      return 'pending';
    };

    const updatedSession = await Session.findOneAndUpdate(
      { name: sessionName },
      {
        $set: {
          users,
          status: getStatus(),
        },
      },
      { new: true },
    );

    eventEmitter.emit(Events.sendMessageToRoom, {
      room: `session-${session?._id}`,
      event: 'message',
      data: {
        value: updatedSession,
        event: 'users',
      },
    });

    return res.status(200).send({ data: updatedSession });
  },
  get: async (req: Request, res: Response) => {
    const { sessionId } = req.params;

    const session = await Session.findOne({ _id: new ObjectId(sessionId) });

    if (!session) {
      return res.status(400).send({ message: 'Session does not exist' });
    }

    return res.status(200).send({ data: session });
  },
  patchPlayer: async (req: Request, res: Response) => {
    const { sessionName, userId } = req.params;

    const { type } = req.body;

    const updatedSession = await Session.findOneAndUpdate(
      {
        name: sessionName,
        'users.id': userId,
      },
      { $set: { 'users.$.type': type } },
      { new: true },
    );

    eventEmitter.emit(Events.sendMessageToRoom, {
      room: `session-${updatedSession?._id}`,
      event: 'message',
      data: {
        value: updatedSession,
        event: 'users',
      },
    });

    return res.status(200).send({ data: updatedSession });
  },
};

export { sessions };
