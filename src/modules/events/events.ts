import { Server } from 'socket.io';
import { Events } from '@shared/enums';

import { eventEmitter } from './emitter';

const events = (io: Server) => {
  io.on('connection', socket => {
    console.log('user connected');

    if (socket.handshake.query.sessionId) {
      socket.join(`session-${socket.handshake.query.sessionId}`);
    }

    socket.on('ping', () => {
      console.log('[ping]');
      io.emit('ping');
    });

    socket.on(
      'vote-councilor',
      ({ sessionId, userId, answer }: { sessionId: string; userId: string; answer: boolean }) => {
        console.log('vote-councilor', sessionId, userId);
        io.to(`session-${sessionId}`).emit('message', {
          value: { userId, answer },
          event: 'vote-councilor',
        });

        setTimeout(() => {
          io.to(`session-${sessionId}`).emit('message', {
            value: { userId: '632a1aa866932740f6ebcdf0', answer },
            event: 'vote-councilor',
          });
        }, 1000);

        setTimeout(() => {
          io.to(`session-${sessionId}`).emit('message', {
            value: { userId: '632a1ab266932740f6ebcdf6', answer },
            event: 'vote-councilor',
          });
        }, 1500);
      },
    );

    socket.on(
      'select-councilor',
      ({ sessionId, userId }: { sessionId: string; userId: string }) => {
        console.log('select-councilor', sessionId, userId);
        io.to(`session-${sessionId}`).emit('message', { value: userId, event: 'select-councilor' });
      },
    );
  });

  eventEmitter.on(Events.sendMessage, ({ event, data }) => {
    io.emit(event, data);
  });

  eventEmitter.on(Events.sendMessageToRoom, ({ room, event, data }) => {
    io.to(room).emit(event, data);
  });
};

export { events };
