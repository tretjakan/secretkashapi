import { NextFunction, Request, Response } from 'express';
import { CustomError } from '../shared/errors';

export const errorHandler = (
  err: TypeError | CustomError,
  req: Request,
  res: Response,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  next: NextFunction,
) => {
  let customError = err;
  console.log('error');
  if (!(err instanceof CustomError)) {
    customError = new CustomError('Something went wrong');
  }

  if (process.env.NODE_ENV !== 'production') {
    console.error(err);
  }

  res.setHeader('Content-Type', 'application/json');
  res.status((customError as CustomError).status).json(customError);
};
