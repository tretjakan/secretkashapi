import { NextFunction, Request, Response } from 'express';
import chalk from 'chalk';

const requestIdHeader = 'x-requestId';

const getActualRequestDurationInMilliseconds = (start: [number, number]) => {
  const NS_PER_SEC = 1e9; //  convert to nanoseconds
  const NS_TO_MS = 1e6; // convert to milliseconds
  const diff = process.hrtime(start);
  return (diff[0] * NS_PER_SEC + diff[1]) / NS_TO_MS;
};

export const requestLogger = (req: Request, res: Response, next: NextFunction): void => {
  const currentDatetime = new Date();
  const formattedDate = currentDatetime.toISOString();
  const { method, url } = req;
  const start = process.hrtime();

  res.on('finish', () => {
    const status = res.statusCode;
    const requestId = req.headers[requestIdHeader];
    const durationInMilliseconds = getActualRequestDurationInMilliseconds(start);
    const log = `[${chalk.green(
      formattedDate,
    )}][received response:${requestId}] ${method}:${url} ${status} ${chalk.red(
      durationInMilliseconds.toLocaleString(),
    )} ms`;
    console.log(log);
  });
  next();
};
