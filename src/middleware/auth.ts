import jwt from 'jsonwebtoken';
import { NextFunction, Request, Response } from 'express';

const authMiddleware = (req: Request, res: Response, next: NextFunction) => {
  try {
    if (!req.headers.authorization) {
      return res.status(401).send({ error: 'No authorization header' });
    }

    const token = req.headers.authorization;

    if (!token) {
      return res.status(401).send({ error: 'Malformed auth header' });
    }

    const payload = jwt.verify(token, process.env.SECRET as string);
    if (!payload) {
      return res.status(401).send({ error: 'Token verification failed' });
    }
    req.user = payload;
    return next();
  } catch (error) {
    return res.status(401).send({ error });
  }
};

export { authMiddleware };
